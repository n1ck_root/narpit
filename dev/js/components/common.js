$(document).ready(function(){
	$('.c-header-tc-mnu ').click(function () {
 		$(".c-header-menu").toggleClass('menu_state_open');
	});
});
$(document).ready(function(){
	$('.c-header-tc-mnu-close').click(function () {
 		$(".c-header-menu").toggleClass('menu_state_open');
	});
});


$('.projects__slider').slick({
	dots: true,
	arrows: false,
  infinite: false,
  speed: 1200,
  slidesToShow: 4,
	slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 993,
      settings: {
        slidesToShow: 2,
				slidesToScroll: 2,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
				slidesToScroll: 1,
				dots: true,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

$('.clients-slider').slick({
	dots: false,
	arrows: false,
  infinite: true,
  speed: 1200,
  slidesToShow: 5,
	slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

$('.reviews-slider').slick({
	dots: true,
	arrows: false,
  infinite: false,
  speed: 600,
  slidesToShow: 2,
	slidesToScroll: 1,
	adaptiveHeight: true,
  responsive: [
    {
      breakpoint: 993,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    }
  ]
});

$(window).scroll(function () {
		if ($(this).scrollTop() > 200) {
				$('#myBtn').fadeIn();
		} else {
				$('#myBtn').fadeOut();
		}
});

$('#myBtn').click(function () {
		$("html, body").animate({
				scrollTop: 0
		}, 1200);
		return false;
});

$(window).on('load', function() { 
  $('#status').fadeOut(); 
  $('#preloader').delay(150).fadeOut('slow'); 
})


$(window).scroll(function() {
	var top = $(document).scrollTop();
	if (top < 0) $(".c-header-fixed").removeClass('fixed-header');
	else $(".c-header-fixed").addClass('fixed-header');
});

$(document).ready(function(){
		$("#c-header-menu").on("click","a", function (event) {
				//отменяем стандартную обработку нажатия по ссылке
				event.preventDefault();
				//забираем идентификатор бока с атрибута href
				var id  = $(this).attr('href'),
				//узнаем высоту от начала страницы до блока на который ссылается якорь
						top = $(id).offset().top - 75;
				//анимируем переход на расстояние - top за 1500 мс
				$('body,html').animate({scrollTop: top}, 1500);
		});
});

var mapElem = $('#map');
function initMap() {
	var map = new google.maps.Map(mapElem[0], {
		center: {
			lat: parseFloat(mapElem.attr('data-lat')),
			lng: parseFloat(mapElem.attr('data-lng'))
		},
		zoom: 13,
		disableDefaultUI: true,
		styles: [
		{
				"featureType": "all",
				"elementType": "labels.text.fill",
				"stylers": [
						{
								"saturation": 36
						},
						{
								"color": "#000000"
						},
						{
								"lightness": 40
						}
				]
		},
		{
				"featureType": "all",
				"elementType": "labels.text.stroke",
				"stylers": [
						{
								"visibility": "on"
						},
						{
								"color": "#000000"
						},
						{
								"lightness": 16
						}
				]
		},
		{
				"featureType": "all",
				"elementType": "labels.icon",
				"stylers": [
						{
								"visibility": "off"
						}
				]
		},
		{
				"featureType": "administrative",
				"elementType": "geometry.fill",
				"stylers": [
						{
								"color": "#000000"
						},
						{
								"lightness": 20
						}
				]
		},
		{
				"featureType": "administrative",
				"elementType": "geometry.stroke",
				"stylers": [
						{
								"color": "#000000"
						},
						{
								"lightness": 17
						},
						{
								"weight": 1.2
						}
				]
		},
		{
				"featureType": "landscape",
				"elementType": "geometry",
				"stylers": [
						{
								"color": "#000000"
						},
						{
								"lightness": 20
						}
				]
		},
		{
				"featureType": "poi",
				"elementType": "geometry",
				"stylers": [
						{
								"color": "#000000"
						},
						{
								"lightness": 21
						}
				]
		},
		{
				"featureType": "road.highway",
				"elementType": "geometry.fill",
				"stylers": [
						{
								"color": "#000000"
						},
						{
								"lightness": 17
						}
				]
		},
		{
				"featureType": "road.highway",
				"elementType": "geometry.stroke",
				"stylers": [
						{
								"color": "#000000"
						},
						{
								"lightness": 29
						},
						{
								"weight": 0.2
						}
				]
		},
		{
				"featureType": "road.arterial",
				"elementType": "geometry",
				"stylers": [
						{
								"color": "#000000"
						},
						{
								"lightness": 18
						}
				]
		},
		{
				"featureType": "road.local",
				"elementType": "geometry",
				"stylers": [
						{
								"color": "#000000"
						},
						{
								"lightness": 16
						}
				]
		},
		{
				"featureType": "transit",
				"elementType": "geometry",
				"stylers": [
						{
								"color": "#000000"
						},
						{
								"lightness": 19
						}
				]
		},
		{
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
						{
								"color": "#000000"
						},
						{
								"lightness": 17
						}
				]
		}
]
	});
}
if (mapElem.length) {
	initMap();
}

// $(document).ready(function () {
// 	$('input,textarea').focus(function(){
// 		$(this).data('placeholder',$(this).attr('placeholder'))
// 		$(this).attr('placeholder','');
// 	});
// 	$('input,textarea').blur(function(){
// 		$(this).attr('placeholder',$(this).data('placeholder'));
// 	});
// });


// $(function frmotpr(){
// 	var field = new Array("username", "phone", "email");
// 	$("#file_form").submit(function() {
// 			var error=0;
// 			$("#file_form").find(":input").each(function() {
// 					for(var i=0;i<field.length;i++){
// 							if($(this).attr("name")==field[i]){
// 									if(!$(this).val()){
// 											$(this).addClass('notvalid');
// 											error=1;    
// 									}
// 									else{
// 											$(this).removeClass('notvalid');
// 									}
// 							}                       
// 					}               
// 			})
// 			var email = $("#email").val();
// 			if(!isValidEmailAddress(email)){
// 					error=2;
// 					$("#email").addClass('notvalid');
// 			}
// 			if(error==0){
// 			return true;
// 			}else{
// 			var err_text = "";
// 			if(error==1)  err_text="Не все обязательные поля заполнены!";
// 			if(error==2)  err_text="Введен не корректный e-mail!";
// 			$("#messenger").html(err_text); 
// 			$("#messenger").fadeIn("slow"); 
// 			return false;
// 			}
// 	})
// });
// function isValidEmailAddress(emailAddress) {
// var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
// return pattern.test(emailAddress);}