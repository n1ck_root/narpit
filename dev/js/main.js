/* frameworks */
//=include ../../dist/bower_components/jquery/dist/jquery.js
//=include ../../dist/bower_components/moff/dist/moff.min.js

/* libs */
//=include lib/modernizr-custom.js
// lib/bootstrap.min.js
//=include ../../dist/bower_components/svg4everybody/dist/svg4everybody.min.js
//=include ../../dist/bower_components/bootstrap/js/dist/util.js
//=include ../../dist/bower_components/bootstrap/js/dist/tab.js
//=include plugins/bs/modal.js
//=include helpers/bs_modal_fix.js

/* plugins */
//=include plugins/slick/slick.min.js
//=include plugins/fancybox/fancybox.min.js
//=include plugins/aos/aos.js
AOS.init();

/* separate */
//=include helpers/object-fit.js
//=include separate/global.js

/* components */
//=include components/common.js
svg4everybody();
// the main code
window.moffConfig = {
	// Website CSS breakpoints.
	// Default values from Twitter Bootstrap.
	// No need to set xs, because of Mobile first approach.
	breakpoints: {
		xs: 0,
		sm: 768,
		md: 992,
		lg: 1220
	},
	loadOnHover: true,
	cacheLiveTime: 2000
};

if ($('.js-form-check').length) {
	Moff.amd.register({
		id: 'validation',
		depend: {
			js: ['bower_components/jquery-validation/dist/jquery.validate.min.js']
		},
		file: {
			js: ['s/js/helpers/valid.js']
		},
	
		beforeInclude: function() {},
			afterInclude: function() {},
			
		loadOnScreen: ['md', 'lg', 'sm', 'xs'],
		onWindowLoad: false
	});
}